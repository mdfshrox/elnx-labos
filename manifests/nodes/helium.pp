# Helium - DNS server
node helium inherits default {
	include bind
	bind::server::file { [ 'chem.net.lan', '64.168.192.in-addr.arpa' ]:
		source_base => 'puppet:///files/bind/',
	}
	
	bind::server::conf { '/etc/named.conf':
		listen_on_addr    => [ 'any' ],
		listen_on_v6_addr => [ 'any' ],
		forwarders        => [ '8.8.8.8', '8.8.4.4' ],
		allow_query       => [ 'localnets' ],
		
	
	zones				  => {
		'chem.net' => [
		'type master',
		'file "chem.net.lan"',
	],
    '64.168.192.in-addr.arpa' => [
      'type master',
      'file "64.168.192.in-addr.arpa"',
    ],
	},
	}

	package { "bind-utils":
			ensure  => installed,
	}
}
