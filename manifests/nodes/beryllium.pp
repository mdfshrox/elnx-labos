# Beryllium - DHCP server
#
node beryllium inherits default {

		class { 'dhcp':
	dnsdomain    => [
	hiera('dns::domain'),
	hiera('dns::reverse_domain'),
	],
	nameservers  => [hiera('helium::ip')],
	ntpservers   => ['be.pool.ntp.org'],
	interfaces   => ['eth1'],
}
dhcp::pool{ 'chem.net':
	network => '192.168.64.0',
	mask    => '255.255.255.0',
	range   => '192.168.64.101 192.168.64.254',
	gateway => '10.0.2.2',
}
dhcp::host {
	'neon': mac => "08:00:27:70:8d:b3", ip => "192.168.64.10";

}

}
